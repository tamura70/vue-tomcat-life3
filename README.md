# vue-tomcat-life1

## 準備

```
sbt compile
cd front
npm install
```

## テスト実行

1. `make`
2. `sbt ~Tomcat/start`
3. <http://localhost:8080/> を開く

### Links

- [Vue.js](https://jp.vuejs.org/v2/guide/)
- https://codepen.io/B_Sanchez/pen/yzzXZo
- [Apache: Tomcat](http://tomcat.apache.org/)
- [GitHub: xsbt-web-plugin](https://github.com/earldouglas/xsbt-web-plugin)
- [GitHub: json4s](https://github.com/json4s/json4s)

