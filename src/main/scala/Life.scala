package web

import org.sat4j.core.VecInt

class LifeSolver(rows: Int, cols: Int, timeout: Int, model: Int) {
  val usePB = true
  val steps = 2
  val solver = new Sat4jPB(timeout*1000)
  def x(t: Int, i: Int, j: Int): Int = {
    require(0 <= t && t < steps)
    rows*cols*t + cols*i + j + 1
  }
  def p(t: Int, i: Int, j: Int): Int = {
    require(0 <= t && t < steps)
    rows*cols*(steps+t) + cols*i + j + 1
  }
  def cells(t: Int): Seq[Int] =
    for (i <- 0 until rows; j <- 0 until cols) yield x(t,i,j)
  def leK(xs: Seq[Int], k: Int): Iterator[Seq[Int]] =
    for (ys <- xs.combinations(k+1)) yield ys.map(- _)
  def geK(xs: Seq[Int], k: Int): Iterator[Seq[Int]] =
    leK(xs.map(- _), xs.size - k)
  def eqK(xs: Seq[Int], k: Int): Iterator[Seq[Int]] =
    leK(xs, k) ++ geK(xs, k)
  def neK(xs: Seq[Int], k: Int): Iterator[Seq[Int]] =
    for (ys <- xs.combinations(k); zs = xs diff ys)
    yield ys.map(- _) ++ zs.toIterator
  def addClauses(clauses: Iterator[Seq[Int]]): Unit =
    for (clause <- clauses) solver.addClause(clause)
  def encodeCell(t: Int, i: Int, j: Int): Unit = {
    val s = for {
      i1 <- i-1 to i+1; j1 <- j-1 to j+1; if i1 != i || j1 != j
      if 0 <= i1 && i1 < rows && 0 <= j1 && j1 < cols
    } yield x(t,i1,j1)
    if (usePB) {
      /*
       x + 2s -  5x' >= 0
       x + 2s + 10x' <= 17
       x + 2s +  8x' -  8p >= 0
       x + 2s - 13x' - 13p <= 4
       */
      val x0 = x(t,i,j)
      val x1 = x(t+1,i,j)
      val p0 = p(t,i,j)
      solver.addAtLeast(
        x0 +: s :+ x1, 
        1 +: Seq.fill(s.size)(2) :+ -5,
        0
      )
      solver.addAtMost(
        x0 +: s :+ x1, 
        1 +: Seq.fill(s.size)(2) :+ 10,
        17
      )
      solver.addAtLeast(
        x0 +: s :+ x1 :+ p0, 
        1 +: Seq.fill(s.size)(2) :+ 8 :+ -8,
        0
      )
      solver.addAtMost(
        x0 +: s :+ x1 :+ p0,
        1 +: Seq.fill(s.size)(2) :+ -13 :+ -13,
        4
      )
    } else {
      // (s<=1 => x'=0) <==> (s>=2 or x'=0)
      addClauses(geK(s, 2).map(_ :+ -x(t+1,i,j)))
      // (s=2 and x=1 => x'=1) <==> (s!=2 or x=0 or x'=1)
      addClauses(neK(s, 2).map(_ :+ -x(t,i,j) :+ x(t+1,i,j)))
      // (s=2 and x=0 => x'=0) <==> (s!=2 or x=1 or x'=0)
      addClauses(neK(s, 2).map(_ :+ x(t,i,j) :+ -x(t+1,i,j)))
      // (s=3 => x'=1) <==> (s!=3 or x'=1)
      addClauses(neK(s, 3).map(_ :+ x(t+1,i,j)))
      // (s>=4 => x'=0) <==> (s<=3 or x'=0)
      addClauses(leK(s, 3).map(_ :+ -x(t+1,i,j)))
    }
  }
  def encodeBoard(t: Int): Unit = {
    for (i <- 0 until rows; j <- 0 until cols)
      encodeCell(t, i, j)
    for (i <- Seq(0,rows-1); j <- 0 to cols-2)
      solver.addClause(Seq(-x(t,i,j), -x(t,i,j+1), -x(t,i,j+2)))
    for (j <- Seq(0,cols-1); i <- 0 to rows-2)
      solver.addClause(Seq(-x(t,i,j), -x(t,i+1,j), -x(t,i+2,j)))
  }
  def encodeCellExtra(t: Int, i: Int, j: Int, d: Int): Unit = {
    val xs = for {
      i1 <- i-d to i+d; j1 <- j-d to j+d;
      if 0 <= i1 && i1 < rows && 0 <= j1 && j1 < cols
    } yield x(t+1,i1,j1)
    solver.addClause(xs :+ -x(t,i,j))
  }
  def encodeExtra(t: Int, d: Int): Unit = {
    for (i <- 0 until rows; j <- 0 until cols)
      encodeCellExtra(t, i, j, d)
  }
  def encodeGoal(t: Int, goal: Seq[Seq[Int]]): Unit = {
    for (i <- 0 until rows; j <- 0 until cols)
      if (goal(i)(j) > 0) solver.addClause(Seq(x(t,i,j)))
      else solver.addClause(Seq(-x(t,i,j)))
  }
  def encode(goal: Seq[Seq[Int]], max: Int): Int = {
    model match {
      case 0 => {
        encodeBoard(0)
        encodeGoal(1, goal)
        if (max > 0)
          solver.addAtMost(cells(0), max)
        0
      }
      case 1 => {
        encodeBoard(0)
        encodeExtra(0, 2)
        encodeGoal(1, goal)
        if (max > 0)
          solver.addAtMost(cells(0), max)
        0
      }
      case 2 => {
        encodeBoard(0)
        encodeExtra(0, 1)
        encodeGoal(1, goal)
        if (max > 0)
          solver.addAtMost(cells(0), max)
        0
      }
    }
  }
  def solution(t: Int, max: Int, minimize: Boolean): (String,Seq[Seq[Int]]) = {
    val (sat, set) =
      if (minimize) {
        val lits = for (i <- 0 until rows; j <- 0 until cols) yield x(t,i,j)
        solver.setObjectiveFunction(lits)
        solver.minimize(max)
      } else {
        solver.solve()
      }
    if (sat == "SAT" || sat == "OPT") {
      val board =
        for (i <- 0 until rows) yield
          for (j <- 0 until cols) yield
            if (set(x(t,i,j))) 1 else 0
      (sat, board)
    } else {
      (sat, Seq.empty)
    }
  }
}

object Life {
  def forward(rows: Int, cols: Int, board: Seq[Seq[Int]]): Seq[Seq[Int]] = {
    def neighborsSum(i: Int, j: Int): Int = {
      val xs = for {
        i1 <- i-1 to i+1; j1 <- j-1 to j+1; if i1 != i || j1 != j
        if 0 <= i1 && i1 < rows; if 0 <= j1 && j1 < cols
      } yield board(i1)(j1)
      xs.sum
    }
    for (i <- 0 until rows) yield {
      for (j <- 0 until cols) yield {
        val s = 2*neighborsSum(i, j) + board(i)(j)
        if (4 < s && s < 8) 1 else 0
      }
    }
  }
  def backward(rows: Int, cols: Int, timeout: Int, model: Int, 
               board: Seq[Seq[Int]], max: Int, minimize: Boolean): (String, Seq[Seq[Int]]) = {
    val life = new LifeSolver(rows, cols, timeout, model)
    val t = life.encode(board, max)
    life.solution(t, max, minimize)
  }
}
