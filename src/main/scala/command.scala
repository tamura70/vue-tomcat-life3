package web

/*
   https://github.com/json4s/json4s
 */

import org.json4s._
import org.json4s.JsonDSL._
import org.json4s.jackson.JsonMethods._

object Command {
  /* https://stackoverflow.com/questions/23348480/json4s-convert-type-to-jvalue */
  def encodeJson(src: AnyRef): JValue = {
    import org.json4s.{ Extraction, NoTypeHints }
    import org.json4s.JsonDSL.WithDouble._
    import org.json4s.jackson.Serialization
    implicit val formats = Serialization.formats(NoTypeHints)
    Extraction.decompose(src)
  }
 
  def handler(command: String, params: Map[String,Seq[String]]): JValue = command match {
    case "date" => {
      val fmt = new java.text.SimpleDateFormat("yyyy/MM/dd HH:mm:ss")
      val str = fmt.format(new java.util.Date)
      JString(str)
    }
    case "life-forward" => {
      val rows: Int = params("m").head.toInt
      val cols: Int = params("n").head.toInt
      val board: Seq[Seq[Int]] = params.get("b") match {
        case Some(Seq(b)) =>
          for (r <- b.split("[^01]+").toSeq) yield
            for (c <- r.split("").toSeq) yield
              if (c == "0") 0 else 1
        case _ =>
          throw new IllegalArgumentException("parameter b is not defined")
      }
      encodeJson(Map("board" -> Life.forward(rows, cols, board)))
    }
    case "life-backward" => {
      val rows: Int = params("m").head.toInt
      val cols: Int = params("n").head.toInt
      val timeout: Int = params.get("t") match {
        case Some(Seq(t)) => t.toInt
        case _ => 0
      }
      val model: Int = params("model").head.toInt
      val board: Seq[Seq[Int]] = params.get("b") match {
        case Some(Seq(b)) =>
          for (r <- b.split("[^01]+").toSeq) yield
            for (c <- r.split("").toSeq) yield
              if (c == "0") 0 else 1
        case _ =>
          throw new IllegalArgumentException("parameter b is not defined")
      }
      val max: Int = params.get("max") match {
        case Some(Seq(max)) => max.toInt
        case _ => 0
      }
      val minimize: Boolean = params.get("minimize") match {
        case Some(Seq(m)) => m == "true"
        case _ => false
      }
      val (sat, board1) = Life.backward(rows, cols, timeout, model, board, max, minimize)
      encodeJson(Map("status" -> sat, "board" -> board1))
    }
    case _ => {
      JNull
    }
  }

  def exec(command: String, params: Map[String,Seq[String]]): Option[String] = {
    println(s"Command.exec: $command, $params")
    val result: JValue = handler(command, params)
    if (result == JNull) {
      None
    } else {
      val json = 
        ("command" -> command) ~
        ("params" -> params) ~
        ("result" -> result)
      Some(compact(render(json)))
    }
  }
}

