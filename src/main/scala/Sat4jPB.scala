package web

import org.sat4j.pb.SolverFactory
import org.sat4j.pb.IPBSolver
import org.sat4j.pb.PseudoOptDecorator
import org.sat4j.specs.IOptimizationProblem
import org.sat4j.specs.ContradictionException
import org.sat4j.specs.TimeoutException
import org.sat4j.core.Vec
import org.sat4j.core.VecInt
import java.math.BigInteger
import scala.collection.BitSet

class Sat4jPB(timeout: Int) {
  val pbSolver = new PseudoOptDecorator(SolverFactory.newDefaultOptimizer())

  def addClause(clause: Seq[Int]): Unit = {
    pbSolver.addClause(new VecInt(clause.toArray))
  }
  def addAtMost(lits: Seq[Int], degree: Int): Unit = {
    pbSolver.addAtMost(new VecInt(lits.toArray), degree)
  }
  def addAtLeast(lits: Seq[Int], degree: Int): Unit = {
    pbSolver.addAtLeast(new VecInt(lits.toArray), degree)
  }
  def addExactly(lits: Seq[Int], degree: Int): Unit = {
    pbSolver.addExactly(new VecInt(lits.toArray), degree)
  }
  def addAtMost(lits: Seq[Int], coeffs: Seq[Int], degree: Int): Unit = {
    pbSolver.addAtMost(new VecInt(lits.toArray), new VecInt(coeffs.toArray), degree)
  }
  def addAtLeast(lits: Seq[Int], coeffs: Seq[Int], degree: Int): Unit = {
    pbSolver.addAtLeast(new VecInt(lits.toArray), new VecInt(coeffs.toArray), degree)
  }
  def addExactly(lits: Seq[Int], coeffs: Seq[Int], degree: Int): Unit = {
    pbSolver.addExactly(new VecInt(lits.toArray), new VecInt(coeffs.toArray), degree)
  }
  def setObjectiveFunction(lits: Seq[Int]): Unit = {
    setObjectiveFunction(lits, Seq.fill(lits.size)(1))
  }
  def setObjectiveFunction(lits: Seq[Int], coeffs: Seq[Int]): Unit = {
    val obj = new org.sat4j.pb.ObjectiveFunction(
      new VecInt(lits.toArray),
      new Vec[BigInteger](coeffs.map(c => new BigInteger(c.toString)).toArray)
    )
    pbSolver.setObjectiveFunction(obj)
  }
  def solve(): (String,BitSet) = {
    var set = BitSet.empty
    try {
      pbSolver.setTimeoutMs(timeout)
      if (pbSolver.isSatisfiable()) {
        val model = pbSolver.model()
        for (i <- 0 until model.length) if (model(i) > 0) set += model(i)
        ("SAT", set)
      } else {
        ("UNSAT", BitSet.empty)
      }
    } catch {
      case e: ContradictionException => {
        ("UNSAT", BitSet.empty)
      }
      case e: TimeoutException => {
        println("TIMEOUT\n")
        ("TIMEOUT", BitSet.empty)
      }
      case e: Exception => {
        println(s"ERROR ${e.getMessage}\n")
        println(e)
        ("ERROR", BitSet.empty)
      }
    } finally {
      pbSolver.setTimeoutMs(0)
      pbSolver.reset
    }
  }
  def minimize(max: Int): (String,BitSet) = {
    var found = false
    var set = BitSet.empty
    if (max > 0)
      pbSolver.forceObjectiveValueTo(new BigInteger(max.toString))
    try {
      pbSolver.setTimeoutMs(timeout)
      if (pbSolver.admitABetterSolution() && pbSolver.nonOptimalMeansSatisfiable()) {
        do {
          val model = pbSolver.model()
          set = BitSet.empty
          for (i <- 0 until model.length) if (model(i) > 0) set += model(i)
          println("Found " + pbSolver.calculateObjective)
          pbSolver.forceObjectiveValueTo(pbSolver.calculateObjective)
          pbSolver.discardCurrentSolution()          
          found = true
        } while (pbSolver.admitABetterSolution())
      }
      if (found) {
        ("OPT", set)
      } else {
        ("UNSAT", BitSet.empty)
      }
    } catch {
      case e: ContradictionException => {
        if (found) {
          ("OPT", set)
        } else {
          ("UNSAT", BitSet.empty)
        }
      }
      case e: TimeoutException => {
        println("TIMEOUT\n")
        if (found) {
          ("SAT", set)
        } else {
          ("TIMEOUT", BitSet.empty)
        }
      }
      case e: Exception => {
        println(s"ERROR ${e.getMessage}\n")
        println(e)
        ("ERROR", BitSet.empty)
      }
    } finally {
      pbSolver.setTimeoutMs(0)
      pbSolver.reset
    }
  }
}
