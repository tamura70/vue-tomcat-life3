package web

import org.sat4j.minisat.SolverFactory
import org.sat4j.specs.ISolver
import org.sat4j.specs.IProblem
import org.sat4j.specs.ContradictionException
import org.sat4j.specs.TimeoutException
import org.sat4j.core.VecInt
import scala.collection.BitSet

class Sat4j(timeout: Int) {
  val sat4jSolver: ISolver = SolverFactory.newDefault()
  val bcEncoder = new org.sat4j.tools.encoding.Sequential()
  // val bcEncoder = new org.sat4j.tools.encoding.Commander()

  def addClause(clause: Seq[Int]): Unit = {
    sat4jSolver.addClause(new VecInt(clause.toArray))
  }
  def addAtMost(lits: Seq[Int], degree: Int): Unit = {
    bcEncoder.addAtMost(sat4jSolver, new VecInt(lits.toArray), degree)
  }
  def addAtLeast(lits: Seq[Int], degree: Int): Unit = {
    bcEncoder.addAtLeast(sat4jSolver, new VecInt(lits.toArray), degree)
  }
  def addExactly(lits: Seq[Int], degree: Int): Unit = {
    bcEncoder.addExactly(sat4jSolver, new VecInt(lits.toArray), degree)
  }

  def solve(assumptions: Seq[Int]): (String,BitSet) = {
    var set = BitSet.empty
    try {
      sat4jSolver.setTimeoutMs(timeout)
      if (sat4jSolver.isSatisfiable(new VecInt(assumptions.toArray))) {
        val model = sat4jSolver.model()
        for (i <- 0 until model.length) {
          if (model(i) > 0)
            set += model(i)
        }
        ("SAT", set)
      } else {
        ("UNSAT", set)
      }
    } catch {
      case e: ContradictionException => {
        ("UNSAT", BitSet.empty)
      }
      case e: TimeoutException => {
        println("TIMEOUT\n")
        ("TIMEOUT", BitSet.empty)
      }
      case e: Exception => {
        println(s"ERROR ${e.getMessage}\n")
        println(e)
        ("ERROR", BitSet.empty)
      }
    } finally {
      sat4jSolver.setTimeoutMs(0)
      sat4jSolver.reset
    }
  }
}
